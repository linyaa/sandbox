source: https://moonbase.lgbt/blog/c-mixed-type-arithmetic/ on 2024-11-25

# Mixed-type arithmetic in C

A lot of people don’t know this one weird trick — much like JavaScript, C also lets you perform arithmetic with mixed types:

```
#include <stdio.h>
int main() {
    puts("-0.5" + 1);
}
```

```
$ gcc code.c && ./a.out
0.5
```

You can do this for exponents, too!

```
#include <stdio.h>
int main() {
    printf("%d\n", 50 ** "2");
}
```

```
$ gcc code.c && ./a.out
2500
```

Ain’t that neat?
